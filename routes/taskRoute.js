const express = require('express');

// Create a router instance that functions as a middleware and routing system

const router = express.Router();

const taskController = require("../controllers/tasksController");

//[Section] ROUTES
// Route to get all the tasks
// This route expects to receive a GET request at the URL "/tasks"


router.get('/', (req,res)=>{
    // Invokes the 'getAllTasks function from the "taskController.js" file and send the result back to the client/Postman
        taskController.getAllTasks().then(
            resultFromController => res.send(resultFromController)); 
});


router.get('/:id', (req,res)=>{
    // Invokes the 'getAllTasks function from the "taskController.js" file and send the result back to the client/Postman
        taskController.getTaskById(req.params.id).then(
            resultFromController => res.send(resultFromController)); 
});

// Route to create a new task
// This route expects to receive a POST request at the URL "/tasks"

router.post("/", (req,res)=>{
    // if information will be coming from the client side the data can be accessed from the request body
    taskController.createTask(req.body).then(resultFromController =>{
        res.send(resultFromController);
    })
})

// Route to delete a task
    // This route expects to receive a DELETE request at the url "/tasks/:id"
    // The oolon (:) is an identifier from the URL, it helps create a dynamic route which allows us to supply information in the URL 
router.delete("/:id", (req,res)=>{
            
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));

})


// Route to Update Task
// This route expects to receive a PUT request at the URL "/tasks/:id"

router.put("/:id", (req,res)=>{

    taskController.updateTask(req.params.id, req.body).then(resultFromController=> res.send(resultFromController))
})

router.put("/:id/:status", (req,res)=>{
    console.log(req.body);
    taskController.updateTaskStatus(req.params.id, req.params.status).then(resultFromController=> res.send(resultFromController))
})


module.exports = router;