// Contains the function and business logic of our Express JS Application
// CRUD Functions
// Meaning all the operations it can do will be placed in this file.


const { find } = require('../models/task');
const Task = require('../models/task');
const router = require('../routes/taskRoute');

// Controller function for getting all tasks 

module.exports.getAllTasks = ()=>{
    return Task.find({}).then(result =>{
        return result
    })
}

module.exports.getTaskById = (taskId)=>{
    return Task.findOne({taskId}).then(result =>{
        return result
    })
}



// Controller function for creating a task

module.exports.createTask = (requestBody)=>{
    
    // Creates a task object based on the Mongoose Model "Task"
    let newTask = new Task({
        name: requestBody.name
    })

    
    // Save the newly created "newTask" object in the mongoDB database

    // newTask.save((saveError, savedTask)=>{}) hindi pala kailangan ito.
    return newTask.save().then((task,error)=>{
        if(error){
            console.log(error);
            return false;
        } else{

           return task; 
        }
    })

    
}


// Controller function for deleting a task
// "taskID" is the URL parameter passed from the "taskRoute.js"
// Business Logic
    /*
        1. Look for the task with the corresponding id provided in the URL/route
        2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
        */

        module.exports.deleteTask = (taskId) =>{
                                                                
            return Task.findByIdAndRemove(taskId).then((removedTask, err)=>{
                if(err){
                    console.log(err);
                    return false;

                }
                else{
                    return removedTask;
                }

            })
        }
        

// Create a controller function for updating a task
// Business Logic
		/*
			1. Get the task with the id using the Mongoose method "findById"
			2. Replace the task's name returned from the database with the "name" property from the request body
			3. Save the task
		*/
    
    module.exports.updateTask = (taskId, newContent) =>{
        
        return Task.findById(taskId).then((result,error)=>{

            if (error){
                console.log(error);
                return false;
            }
                result.name = newContent.name;

                return result.save().then((updatedTask, saveErr)=>{
                    if (saveErr){
                        console.log(saveErr)
                        return false
                    }
                    else{
                        return updatedTask
                    }
                })
        })
    }

    module.exports.updateTaskStatus = (taskId, taskStatus) =>{

        return Task.findById(taskId).then((result,error)=>{

            if (error){
                console.log(error);
                return false;
            }
                result.status = taskStatus;

                return result.save().then((updatedTask, saveErr)=>{
                    if (saveErr){
                        console.log(saveErr)
                        return false
                    }
                    else{
                        return updatedTask
                    }
                })
        })
    }